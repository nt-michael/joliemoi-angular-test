import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductDTO } from '../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsServicesService {

  constructor(private httpClient: HttpClient) { }

  /**
   * @author Michaël Nde
   * @description Searches brand, name, and ingredients for LIKE values
   * @param {string} q Brand or ingredient name to be queried
   * @returns 
   */
  searchProduct(q: string): Observable<HttpResponse<any>> {
    return this.httpClient.get<HttpResponse<any>>(`${environment.baseUrl}/product?q=${q}`);
  }
}
