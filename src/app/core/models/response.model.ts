export interface ProductDTO {
  id: number;
  brand: string;
  name: string;
  ingredient_list: string[];
}
