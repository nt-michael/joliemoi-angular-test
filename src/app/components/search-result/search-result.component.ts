import { Component, Input, OnInit } from '@angular/core';
import { ProductDTO } from 'src/app/core/models/response.model';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  @Input()
  searchResult: ProductDTO[] = [];

  @Input() isLoading:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

}
