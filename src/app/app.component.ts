import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ProductDTO } from './core/models/response.model';
import { ProductsServicesService } from './core/services/products-services.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'joliemoi-angular';
  searchFormGroup: FormGroup = this.fb.group({
    query: ['', Validators.required],
  });
  producResponseData: ProductDTO[] = [];
  isLoading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private productService: ProductsServicesService
  ) {}

  /**
   * @author Michaël Nde
   * @description This method is used to search for products based on the query form parameter.
   * @returns {void}
   */
  onSubmit(): void {
    console.log(this.f.query.value);
    if (this.f.query.value === '') {
      this.toastr.info(`Please enter a product name to search...`, '', {
        closeButton: true,
      });
      return;
    }
    this.isLoading = true;

    this.productService.searchProduct(this.f.query.value).subscribe(
      (res: any) => {
        this.isLoading = false;
        this.producResponseData = res;
        if (this.producResponseData.length === 0)
          this.toastr.info(`No product found!`, '', { closeButton: true });
      },
      (err: any) => {
        this.isLoading = false;
        console.log(err);
      }
    );
  }

  /**
   * @author Michaël Nde
   * @description Search form getter.
   */
  get f() {
    return this.searchFormGroup.controls;
  }
}
